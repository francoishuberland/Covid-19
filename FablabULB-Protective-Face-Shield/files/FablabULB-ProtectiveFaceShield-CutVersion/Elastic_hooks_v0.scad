/*
//* FILE   : anti_projection_2D-full_v0.scad
//* AUTHOR : Denis Terwagne
//* DATE   : 2020-03-20
//* Version: 0.0 (first draft)
//* Notes  : inspired from the 3D model of Nicolas De Coster
//* License : M.I.T. (https://opensource.org/licenses/MIT)
//*
//*/


//tolerance
t=0;

out_d   = 30; //120;  //inside diameter of outer ring
in_d    = 100; //100;  //inside diameter of inner ring
length_ir = 220; //length inner ring
length_or_slots = 280; //length outer ring to slots
length_or = 300; //length outer ring
height  = 10;   //height of the module
//sec     = 5;    //setback from joining branches
th      = 1;    //thickness of module
n_holes = 10;   //number of holes in inner ring
//hook_d  = 10;   //hooks diameter
height_slot = height/2;
width_slot = th*2;
length_tab = 4;//2; //length of the tab

holes_d = height/2;

// Elastic hooks

elastic_hook();

module elastic_hook(){

translate([height,0,0]) 
difference(){
union(){
translate([0,height/2,0])cylinder(r=height/2,$fn=100);
translate([-height,0,0])cube([height,height,th]);
}
union(){
hull(){
translate([0,height/2,0])cylinder(r=height/5,$fn=100);
translate([-height/2,height/2,0])cylinder(r=height/5,$fn=100);
}
hull(){
translate([-height/2,height/2,0])cylinder(r=height/5,$fn=100);
translate([-height/2,height,0])cylinder(r=height/5,$fn=100);
}
}
}
}

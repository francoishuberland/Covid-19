# Anti projection

This a very simple and first test of 3D printable structure to fix transparent 

## Known issues and problems to be fixed

The clip solution obviously isn't a good one. We are investigation other 
external clips or in-design clips.

## Version 2.0

The version 2.0 allows automatic added dashed for stacking. The .stl files seems to have a manyfold problem with Slic3r, but Slic3r can repare it easily (record to an .obj file for better security).

Then add enforcers cylinder in Slic3r to support the pin on a vertical structure.

![](stack.png)

## Overview

foil.

![](openscad.png)

![](shield.jpeg)

## SCAD file

[anti_projection.scad](./anti_projection.scad)

It was written fully parametric. To modify thickness, height, etc. just change 
the corresponding values in the file header.


/*
//* FILE   : anti_projection.scad
//* AUTHOR : Nicolas H.-P. De Coster (Vigon) <ndcoster@meteo.be>
//* DATE   : 2020-03-20
//* Version: 2.0 (Stackable)
//* Notes  :
//*  - 0.0 : first draft : clipping system
//*  - 0.1 : second draft : added pins for foil clipping
//*  - 0.2 : adapted pins size and position
//*  - 1.0 : added central pin (was avoided previously because more complex punch in transaprent foil but asked explicitely by St Pierre Hospital, thus added)
//*  - 1.1 : lowered pins (limiting support)
//*  - 2.0 : stackable version, automatically calculated dashet support
//*  - 2.1 : corrected bug : no more parity issue with dash line
//*  - 2.3 : corrected bug : proper hook support
//
//* License : M.I.T. (https://opensource.org/licenses/MIT)
//*
//*/

//General params
e=0.01;
n_stacked = 15;    //vertical repeat
stacked_d = 0.7;  //distance between stacked pieces (better use integer multiple of your layer height)

//Dashes (support for stacking)
dash_l    = 0.8;    //dash length
dash_int  = 6;    //dash interval

//Main parameters
out_d     = 115;  //inside diameter of outer ring
in_d      = 95;   //inside diameter of inner ring
height    = 8;    //height of the module
sec       = 5;    //setback from joining branches
th        = 1.2;  //thickness of module (=0.4*3 or 0.6*2 for printing speed optimization)
n_holes = 25;     //number of holes in inner ring
hook_d  = 4;      //hooks diameter


//Fixing pins
fix_dist = 297 - 2*10; // (12 seems to be the standard distance from edge on a paper punch)
fix_d = 3 ;       //(6 is supposed to be the standard https://en.wikipedia.org/wiki/Hole_punch)
fix_h = 3.5;      //
fix_over = 1.2;


out_r = out_d/2;
in_r  = in_d/2;
hook_r = hook_d/2;
holes_d = height/2;
fix_r = fix_d/2;

for(i=[0:n_stacked-1]){
    
translate([0,0,i*(height+stacked_d)])
union(){

  structure(out_d, in_d, hook_d, height, th);

  arc_l = PI*(out_d+2*th)*3/4;

  /*
  // Fixings
  */
  translate([out_r+th, (fix_dist-arc_l)/2,height/2])
    rotate([0,90,0])
      fixing(d=fix_d, h=fix_h, over=fix_over);
  translate([(fix_dist-arc_l)/2,out_r+th,height/2])
    rotate([-90,0,0])
      fixing(d=fix_d, h=fix_h, over=fix_over);
  translate([0,0,height/2])
    rotate([-90,0,135])
    translate([0, 0, out_r ])
      fixing(d=fix_d, h=fix_h+th, over=fix_over);

    

}
//support
if(i != n_stacked-1){
  translate([0,0,i*(height+stacked_d)+height])
    structure(out_d, in_d, hook_d, stacked_d, th, isStruct=true);
}
}

/*////////////////
// M O D U L E S 
*/////////////////
module empty_cyl(in_d, th, h, dashed=false){
  difference(){
    cylinder(d=in_d+2*th, h=h);
    translate([0,0,-e])cylinder(d=in_d, h=h+2*e);
    if(dashed){
        circ = PI*(in_d+th/2);
        echo("circ:", circ);
        n_cube_dash = ceil(circ/dash_int);
        echo(n_cube_dash);
        echo(dash_int);
        alpha = 360/n_cube_dash;
        for(i=[0:n_cube_dash-1]){
            rotate([0,0,i*alpha+30]) //+30 cheating to get proper hooks support (dirty but works)
            translate([in_d/2+th,0,h/2])
              cube([in_d/2+th+e, dash_int-dash_l, h+2*e], center=true);
        }
    }
  }
}

module temple(l,w,h,dashed=false){
  difference(){
    cube([l,w,h]);
    if(dashed){
      n_dash = ceil(l/dash_int);
      for(i=[1:n_dash]){
        translate([dash_l+dash_int*i,-e,-e])cube([dash_int-dash_l, w+2*e, h+2*e]);
      }
    }
  }
}

module arc3_4(in_d, th, h, dashed=false){
  difference(){
    empty_cyl(in_d, th, h, dashed=dashed);
    translate([0,0,-e])cube([in_d, in_d, h+2*e]);
  }
}

module clip(h,w,th,clip_solid){
  difference(){
    cube([w,h,clip_solid]);
    translate([th,th,-e/2])cube([th,h-2*th, clip_solid+e]);
    translate([w-2*th,th,-e/2])cube([th,h-2*th, clip_solid+e]);
    translate([th,h-2*th,-e/2])cube([w-2*th,th, clip_solid+e]);
    translate([3*th,2*th,-e/2])cube([th, h-5*th, clip_solid+e]);
    translate([3*th,th,-e/2])cube([w-6*th, th, clip_solid+e]);
    translate([3*th,2*th+(h-8*th)/3,-e/2])cube([w-6*th, th, clip_solid+e]);
    translate([3*th,2*th+2*((h-8*th)/3)+th,-e/2])cube([w-6*th, th, clip_solid+e]);
    translate([3*th,h-4*th,-e/2])cube([w-6*th, th, clip_solid+e]);
  }
    
}

module fixing(d, h, over){
    sph_d = d+2*over;
    cylinder(d=d, h=h-d/2, $fn=50);
    translate([0,0,h-d/2])difference(){
      sphere(d=sph_d, $fn=50);
    translate([0,0,-sph_d/2])cube([sph_d,sph_d,sph_d], center=true);
    }
}

module structure(out_d, in_d, hook_d, height, th, isStruct=false){
    out_r = out_d/2;
    in_r = in_d/2;
    hook_r = hook_d/2;
    arc_dist = 3*PI*in_d/4;
    n_holes = floor(arc_dist/height);
/*
// Arcs
*/
translate()arc3_4(out_d, th, height, dashed=isStruct,$fn=100);
difference(){
  translate([out_r-in_r, out_r-in_r, 0])arc3_4(in_d, th, height, dashed=isStruct, $fn=100);
  if(!isStruct){
    for(i=[1:n_holes+1]){
      translate([out_r-in_r, out_r-in_r, holes_d])
      rotate([90,0,90-i*270/(n_holes+2)])
        cylinder(d=holes_d, h=in_d+th+e, $fn=6);
    }
  }

}

/*
// Temples
*/
translate([0, out_r, 0])
  temple(out_r+th-sec, th, height, dashed=isStruct);
translate([out_r+th, 0, 0])
  rotate([0,0,90])
  temple(out_r+th-sec, th, height, dashed=isStruct);

/*
// Hooks
*/
translate([hook_r+out_r+th,out_r-sec,0])
  rotate([0,0,180])
    arc3_4(hook_d, th, height, dashed=isStruct, $fn=100);
translate([out_r-sec, hook_r+out_r+th,0])
  rotate([0,0,180])
    arc3_4(hook_d, th, height, dashed=isStruct,$fn=100);

arc_l = PI*(out_d+2*th)*3/4;
}
# Fablabs x Covid-19

Projets open-source du Fablab ULB en lien avec la crise du Covid-19.

* [Fablab ULB - Protective Face Shield](./FablabULB-Protective-Face-Shield)

Liens utiles :

* [Prusa - Protective Face Shield](./Prusa-Protective-Face-Shield)
* [EasyMask](https://www.ease-designers.com/easymask-diy-design)
